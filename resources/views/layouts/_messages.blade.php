@if (session('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Success!</strong> {{ session('success') }}
        <button class="close" data-dissmiss="alert" area-label="close">
            <span area-hidden="true">&times;</span>
        </button>
    </div>
@endif
